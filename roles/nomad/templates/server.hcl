data_dir             = "/var/lib/nomad"
disable_update_check = true
enable_syslog        = true

server {
  enabled          = true
  bootstrap_expect = {{  groups['nomad_servers'] | length }}
}


consul {
    address = "127.0.0.1:8500"
    ssl = false
}
