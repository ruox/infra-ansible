data_dir             = "/var/lib/nomad"
disable_update_check = true
enable_syslog        = true

client {
    enabled = true
}

consul {
    address = "127.0.0.1:8500"
    ssl = false
}
