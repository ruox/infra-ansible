"datacenter" = "hiccup"
"data_dir" = "/var/consul"
"retry_join" = {{ cluster_members }}
"client_addr" = "0.0.0.0"
"bind_addr" = "{{ '{{GetInterfaceIP \\"eth0\\" }}' }}"
"leave_on_terminate" = true
"enable_syslog" = true
"disable_update_check" = true
"enable_debug" = true
